var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
var { success, errorProcess, errorWithMess, successWithNoData} = require('services/returnToUser');

module.exports = router => {
  router.post("/password", async (req, res, next) => {
    try {
      if(req.body.newPass === req.body.confirmPass) {
        let user = await mongoose.model('users').findById(req.user._id);
        if (bcrypt.compareSync(req.body.currentPass, user.password)) {
          let hashPass = bcrypt.hashSync(req.body.newPass, bcrypt.genSaltSync());
          user.password = hashPass;
          user.save();
          return successWithNoData(res, "Update thành công");
        } else {
          return errorWithMess(res, "Mật khẩu hiện tại không đúng!");
        }    
      } else {
        return errorWithMess(res, "Mật khẩu mới mà trường nhập lại mật khẩu không khớp");
      }
    } catch (err) { 
      errorProcess(res, err);
    }
  });
};
