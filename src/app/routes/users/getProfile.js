module.exports = router => {
  router.get("/profile", async (req, res, next) => {
    try {
      return res.render('users/profile');
    } catch (err) {
      next(err);
    }
  });
};
