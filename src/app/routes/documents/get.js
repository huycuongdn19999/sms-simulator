var mongoose = require("mongoose");

module.exports = router => {
  router.get("/", async (req, res, next) => {
    try {
      let documents = await mongoose.model("documents").find().sort({ createdDate: -1})
      return res.render("documents/index", { documents });
    } catch (err) {
      next(err);
    }
  });
};
