var mongoose = require('mongoose');

module.exports = router => {
    router.delete('/:id', async (req, res, next) => {
        try {
            await mongoose.model('documents').findByIdAndDelete(req.params.id);
            return res.redirect('/documents')
        } catch (err) {
            next(err);
        }
    })
}