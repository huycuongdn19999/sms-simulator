var mongoose = require("mongoose");

module.exports = router => {
  router.get("/:id", async (req, res, next) => {
    try {
      let documents = await mongoose.model("documents").findById(req.params.id).populate('createdBy').populate('feedback.belongTo')
      console.log(documents)
      return res.render("documents/detail", { documents });
    } catch (err) {
      next(err);
    }
  });
};
