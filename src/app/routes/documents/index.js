var router = require('express').Router();

require('./get')(router);
require('./create')(router);
require('./getId')(router);
require('./delete')(router);

module.exports = router;