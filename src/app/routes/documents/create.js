var { UPLOAD_FOLDER_NAME } = require('config/constants')
var uploadFile = require("services/uploadFile");
var mongoose = require('mongoose');

module.exports = router => {
  router.post("/", uploadFile.StoreFile().any(), async (req, res, next) => {
    try {
      if (req.files.length > 0) {
        req.body.documentLink = `${req.protocol}://${req.get('host')}` + UPLOAD_FOLDER_NAME + "/" + req.files[0].filename;
      }
      req.body.createdBy = req.user
      await mongoose.model('documents').create({ ...req.body })
      return res.redirect('/documents')
    } catch (err) {
      next(err);
    }
  });
};
