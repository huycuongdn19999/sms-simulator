var mongoose = require('mongoose');

module.exports = router => {
    router.delete('/:id', async (req, res, next) => {
        try {
            await mongoose.model('todoList').findByIdAndDelete(req.params.id);
            return res.redirect('/todo-list')
        } catch (err) {
            next(err);
        }
    })
}