var mongoose = require('mongoose');
var { success } = require('services/returnToUser')

module.exports = router => {
    router.post('/:id', async (req, res, next) => {
        try {
            let query = {
                _id: req.params.id
            }
            let update = {
                $push: {
                    tasks: {
                        ...req.body
                    }
                }
            }
            let option = { new: true }
            await mongoose.model('todoList').findOneAndUpdate(query, update, option)
            return res.redirect(`/todo-list/${req.params.id}`)
        } catch (err) {
            next();
        }
    })
}