var mongoose = require('mongoose');

module.exports = router => {
    router.get('/:id', async (req, res, next) => {
        try {
            let todoList = await mongoose.model('todoList').findById(req.params.id).populate('tasks.assignTo');
            let users = await mongoose.model('users').find();
            return res.render('todoList/detail', { todoList, users })
        } catch (err) {
            next(err);
        }
    })
}