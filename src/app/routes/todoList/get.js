var mongoose = require('mongoose');

module.exports = router => {
    router.get("/", async (req, res, next) => {
        try {
            let todoList = await mongoose.model('todoList').find().select({
                _id: 1,
                startTime: 1,
                endTime: 1
            })
            return res.render('todoList/index', { todoList })
        } catch (err) {
            next(err);
        }
    })
}