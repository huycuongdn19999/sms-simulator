var mongoose = require('mongoose');
var { success } = require('services/returnToUser')

module.exports = router => {
    router.post('/:id/:taskId/edit', async (req, res, next) => {
        try {
            let query = {
                _id: req.params.id,
                'tasks._id': req.params.taskId
            }
            let update = {
                $set: {
                    "tasks.$": {
                        ...req.body
                    }
                }
            }
            let option = { new: true }
            let task = await mongoose.model('todoList').findOneAndUpdate(query, update, option)
            return res.redirect(`/todo-list/${req.params.id}`)
        } catch (err) {
            next(err);
        }
    })
}