var router = require('express').Router();

require('./deleteTask')(router);
require('./get')(router);
require('./createToDoList')(router);
require('./getViaId')(router);
require('./createTask')(router);
require('./delete')(router);
require('./getTasks')(router);
require('./editTask')(router);

module.exports = router;