var mongoose = require('mongoose');
var { success } = require('services/returnToUser')

module.exports = router => {
    router.delete('/:id/:taskId', async (req, res, next) => {
        try {
            let query = {
                _id: req.params.id,
                "tasks._id": req.params.taskId
            }
            let update = {
                $pull: {
                    tasks: {
                        _id: req.params.taskId
                    }
                }
            }
            let option = { new: true }
            let task = await mongoose.model('todoList').findOneAndUpdate(query, update, option)
            return success(res, "Done", task)
        } catch (err) {
            next(err);
        }
    })
}