var mongoose = require('mongoose');

module.exports = router => {
    router.post('/', async (req, res, next) => {
        try {
            let todoList = await mongoose.model('todoList').create({
                startTime: req.body.startTime,
                endTime: req.body.endTime
            })
            return res.redirect(`/todo-list/${todoList._id}`)
        } catch (err) {
            next(err);
        }
    })
}