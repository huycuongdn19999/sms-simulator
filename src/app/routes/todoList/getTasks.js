var mongoose = require('mongoose');
var { success } = require('services/returnToUser')
module.exports = router => {
    router.get('/:id/:taskId', async (req, res, next) => {
        try {
            let task = await mongoose.model('todoList').findOne({
                _id: req.params.id,
                'tasks._id': req.params.taskId
            }, {
                'tasks.$': 1
            })
            return success(res, "Done", task)
        } catch (err) {
            next(err);
        }
    })
}