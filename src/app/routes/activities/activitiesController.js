var Activities = require('../../../model/schema').activities;
var ActivitiesJoined = require('../../../model/schema').activitiesJoined;
var qrcode = require('qrcode');
var { notifyAll, notifyToWard } = require('services/notifyToMobile')
var { QR_IMG_DIR } = require('config/constants')
var mongoose = require('mongoose');
var { success, errorProcess, errorWithMess, successWithNoData} = require('services/returnToUser');

//handle insert 
exports.insert = async (req, res) => {
    try {
        let userId = req.user._id;
        let activity = await mongoose.model('activities').create({
            ...req.body,
            createdBy: userId,
            updatedBy: userId,
            updatedDate: new Date()
        });

        let data = {
            activities: activity._id,
            text: activity.name
        }

        await notifyAll(data, data.text)

        return success(res, 'Activities inserted', activity);
    } catch (err) {
        console.log(err);
        return  errorProcess(res, err);
    }
}

//handle get Activities by id
exports.getById = async function (req, res) {
    let id = req.params.activities_id;
    if (!id) {
        errorWithMess(res, 'Activity id invalid');
    } else {
        let data = await Activities.findById(id).
            populate({ 
                path: 'createdBy',
                select: 'username'
            }).
            populate({
                path: 'updatedBy',
                select: 'username'
            });
            
         res.json(data);
    }  
}

//hanle update 
exports.update = async function (req, res) {
    try {
        let id = req.params.activities_id;

        if (!id) { 
            // reject error if id is empty.
            errorWithMess(res, 'Activity id invalid');

        } else {
            let updatedActivity =  await Activities.findById(id);
       
            if (updatedActivity) {

                // Update data for activity
                updatedActivity.name = req.body.name;
                updatedActivity.updatedDate = new Date();
                updatedActivity.updatedBy = req.user._id;
                updatedActivity.description = req.body.description;
                
                // Save to db.
                updatedActivity.save();

                // res success.
                return successWithNoData(res, 'Activities has beeen updated');
            } else {
                return errorWithMess(res, 'Activity id invalid');
            } 
        }
       
    } catch (err) {
        errorProcess(res, err);
    }
}


//handle delete
exports.delete = async function (req, res) {
    try {
        let id = req.params.activities_id;
        if (!id) {
            errorWithMess(res, 'Activity id invalid');
        } else {
            let activities = await Activities.findById(id);
            
            if (activities) {
                // Delete found activity.
                await activities.delete();
                // Delete all activityJoined refer to this activity.
                await ActivitiesJoined.deleteOne({activity:  mongoose.Types.ObjectId(id)});

                successWithNoData(res, 'Activities deleted');
            } else {
                errorWithMess(res, 'Activity id invalid');
            }
        }
       
    } catch (err) {
        console.log(err);
        errorProcess(res, err);
    }
}

//handle export qr code
exports.getQR = async function (req, res) {
    try {
        // Get activity Id
        var id = req.params.id;
        if (!id) {
            errorWithMess(res, 'Activity id invalid');
        } else {
            
             // Save qr img path;
            var imgPath =  QR_IMG_DIR + id + '.png';

            // Client img path.
            var imagePathToClient = '/img/qrcode' + id + '.png';

            // Find activity in db.
            let activity = await Activities.findById(id);

            if (activity) {
                await qrcode.toFile(imgPath, id, { width: 1000, height: 1000 });
                success(res, 'create qr code success', imagePathToClient);
            } else {
                errorWithMess(res, 'Activity id invalid');
            } 
        }
              
    } catch (err) {
        console.log(err);
        errorProcess(res, err);
    }   
}
