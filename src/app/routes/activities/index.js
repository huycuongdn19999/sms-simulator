var router = require("express").Router();
var mongoose=require('mongoose');
var ActivitiesController = require('./activitiesController');

require('./getAll')(router);
require('./getCheckinHistory')(router);

//api routes
router.route('/api')
    .post(ActivitiesController.insert);

router.route('/api/:activities_id')
    .get(ActivitiesController.getById)
    .put(ActivitiesController.update)
    .delete(ActivitiesController.delete);

router.route('/api/qrcode/:id')
    .get(ActivitiesController.getQR);


//update activity page 
router.get('/edit/:id',function(req,res){
    var id = req.params.id;
    res.render('activities/edit',{id});
});

//insert activity page
router.get('/insert',function(req,res){
    res.render('activities/insert');
})


//wards joined page
router.get('/diemdanh',async (req,res)=>{
    try {
        let wardsjoined = await mongoose.model('activitiesJoined').find().populate('activity');
        return res.render('activities/diemdanh',{wardsjoined});
    } catch (err) {
        console.log(err);
        next(err);
    }    
})

router.get('/wardsjoined/:wardjoinedID',async(req,res)=>{
    try {

        let ward = await mongoose.model('activitiesJoined')
                    .findById(req.params.wardjoinedID)
                    .populate({path:'activity', select:'name'})
                    .populate({path:'wardJoined.ward',select:'name'})
        
        res.render('activities/wardsjoinedDetail',{ward});
    } catch (err) {
        next(err);
    }
   
})

module.exports = router;