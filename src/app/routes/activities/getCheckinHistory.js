var {DEFAULT_LIMIT_DATA_PER_REQUEST} = require('../../../config/constants');
var mongoose = require('mongoose')

module.exports = router => {
  router.get('/:activityId/checkin-history', async (req, res, next) => {
    try {   
      let activityId = req.params.activityId;
      let wardId = req.query.w || 'all';
      
      let pipeline = [ 
        { $match: {activity: mongoose.Types.ObjectId(activityId)}}, 
        { $project: { _id: 0, wardJoined: 1 }},
        { $unwind: { path: '$wardJoined', preserveNullAndEmptyArrays: true}},
        { $unwind: { path: '$wardJoined.userJoined', preserveNullAndEmptyArrays: true }}, 
        { $project: { 'wardJoined.numberJoined': 0, 'wardJoined._id': 0, 'wardJoined.userJoined._id': 0 }},
        { $group: { _id: '$wardJoined.ward', userJoined: {$push: '$wardJoined.userJoined'}}}
      ]

      if (wardId != 'all' && mongoose.Types.ObjectId.isValid(wardId)) {
        pipeline.push({ $match: { _id: mongoose.Types.ObjectId(wardId)}})
      }

      pipeline.push(
        { $lookup: { from: 'wards', localField: '_id', foreignField: '_id', as: 'wardInfo'}},
        { $unwind: { path: '$wardInfo' }}
      );

      let activityJoined = await mongoose.model('activitiesJoined').aggregate(pipeline);

      let wards = await mongoose.model('wards').find();
      res.render('activities/checkinHistory', {wards, activityJoined, wardId, activityId})
    } catch (err) {
      console.log(err);
      next(err);
    }
  });
}