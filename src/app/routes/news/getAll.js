var mongoose = require('mongoose');

module.exports = router => {
    router.get('/', async (req, res, next) => {
        try {
            let categoryId = req.query.c || '';
            let categoryFilter = {}

            if (categoryId !== '' && mongoose.Types.ObjectId.isValid(categoryId)) {
                categoryFilter.category = mongoose.Types.ObjectId(categoryId);
            }
            
            let news = await mongoose.model('news').find(categoryFilter).
                        populate({
                            path: 'createdBy',
                            select: 'username'
                        }).
                        populate({
                            path: 'updatedBy',
                            select: 'username'
                        }).
                        sort({ createdDate: -1 }).
                        exec();

            let categories = await mongoose.model('categories').find()
            let wards = await mongoose.model('wards').find();

            return res.render('news/index', { news, categories, wards, categoryId})
        } catch (err) {
            console.log('News@getAll occurred erred');
            console.log(err);
            next(err)
        }
    })
}