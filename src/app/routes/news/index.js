var router = require("express").Router();
var uploadFile = require("services/uploadFile");
var NewsController = require('./newsController');
var { UPLOAD_FOLDER_NAME } = require('config/constants')

require('./getAll')(router);

//api routes
router.route('/api/:news_id')
    .get(NewsController.getById)
    .delete(NewsController.delete);

//handle insert
router.post('/', uploadFile.StoreFile().any(), async (req, res, next) => {
    try {
        if (req.files.length >  0) {
            req.body.imageLink = UPLOAD_FOLDER_NAME + '/' + req.files[0].filename;
        }
        await NewsController.insertRender(req, res);
    } catch (err) {
        next(err);
    }

});

//handle update
router.post('/update/:news_id', uploadFile.StoreFile().any(), async (req, res, next) => {
    try {
        if (req.files.length > 0) {
            req.body.imageLink = UPLOAD_FOLDER_NAME + '/' + req.files[0].filename;
            await NewsController.update(req, res);
        } else {
            req.body.imageLink='';
            await NewsController.update(req, res);
        }
    } catch (error) {
        next(error)
    }

})
module.exports = router;