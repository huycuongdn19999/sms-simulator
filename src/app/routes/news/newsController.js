var mongoose = require('mongoose');
var { success, errorProcess } = require('services/returnToUser');
var uploadFile = require("services/uploadFile");
var { notifyAll, notifyToWard } = require('services/notifyToMobile')
var _ = require('lodash');

//handle insert new
exports.insertRender = async (req, res) => {
  try {
    
    let news = await mongoose.model('news').create({ ...req.body, createdBy: req.user._id });
   
    let dbwards = await mongoose.model('wards').find();

    let wards = _.get(req.body, "wards", []);
    if (!_.isArray(wards)) {
      wards = [wards];
    }

    if (dbwards.length === wards.length) {
      let data = {
        id: news._id,
        text: news.title,
        type: "News"
      }
      await notifyAll(data, data.text)
    } else {            
      wards.map(async item => {
        let data = {
          id: news._id,
          text: news.title,
          type: "News"
        }
        await notifyToWard(data, data.text, item, data)
      })
    }
    
    return res.redirect('/news')
  } catch (err) {
    return next(err)
  }
};

//handle get news by id
exports.getById = async (req, res) => {
  try {
    let news = await mongoose.model('news').findById(req.params.news_id).populate('category')
    return success(res, "Done", news)
  } catch (err) {
    return errorProcess(res, err)
  }
};

//hanle update
exports.update = async (req, res) => {
  try {
    let updatedNews = await mongoose.model('news').findById(req.params.news_id);
    
    if (updatedNews) {
      // Set new value for updatedNews.
      updatedNews.title = req.body.title;
      updatedNews.content = req.body.content; 
      updatedNews.category = req.body.category;
      updatedNews.shortDescription = req.body.shortDescription;

      // If has new img => reset imageLink.
      if (req.body.imageLink !== '') {
        updatedNews.imageLink = req.body.imageLink;
      } 
      
      updatedNews.save();
    }

    return res.redirect('/news')
  } catch (err) {
    return errorProcess(res, err)
  }
  
};

//handle delete
exports.delete = async (req, res) => {
  try {
    await mongoose.model('news').findByIdAndDelete(req.params.news_id);
    return success(res, "Done", null)
  } catch (err) {
    return errorProcess(res, err)
  }
};
