var mongoose = require('mongoose');
var { success, errorProcess } = require('services/returnToUser');
module.exports = router => {
    router.post('/', async (req, res, next) => {
        try {           
            let categories = await mongoose.model('categories').create({ ...req.body });
            if(req.body.type === 'ajax') {
                return success(res, 'Done', categories);
            }

            return res.redirect(`/categories`)
        } catch (err) {
            if(req.body.type === 'ajax') {
                return errorProcess(res, err);
            }
            
            next(err)
        }
    });

    router.post('/:id', async (req, res, next) => {
        try {
            let exams = await mongoose.model('categories').findByIdAndUpdate(req.params.id, { ...req.body });
            return res.redirect(`/categories`)
        } catch (err) {
            next(err)
        }
    })    
}