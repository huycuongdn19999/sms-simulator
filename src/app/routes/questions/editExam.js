var mongoose = require('mongoose');
var { success, errorWithMess} = require('services/returnToUser')
module.exports = router => {
  router.post('/exams/edit', async (req, res, next) => {
    try {
      let id = req.body.id;
      let name = req.body.name;
      let description = req.body.description;
      let updateExam = await mongoose.model('exams').findById(id);
      
      if (updateExam) {
        updateExam.name = name;
        updateExam.description = description;
        updateExam.save();        

        return res.redirect(`/questions`)
      } else {
        throw "/exams/edit - Exams id not valid";
      }
    } catch (err) {
      next(err)
    }
  });
}