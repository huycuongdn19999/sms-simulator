var mongoose = require('mongoose');
var {errorProcess, successWithNoData, errorWithMess} = require('services/returnToUser');

module.exports = router => {  
  router.put('/exams/:id/status/:status/change-status',  async (req, res) => {
      try {
      let id = req.params.id;
      let status = req.params.status;
      let publishedExams = await mongoose.model('exams').findById(id);  
      if (publishedExams) {
        if (status === "true") {
          publishedExams.isPublish = true;
        } 
        else if( status === "false" ) {
          publishedExams.isPublish = false;
        } else {
          return errorWithMess(res, "Status invalid");
        }
        publishedExams.save();
        return successWithNoData(res, "Done!");
      } else {
        return errorWithMess(res, "news' Id not existed")
      }
    } catch (err) {
      return errorProcess(res, err);
    }
    
  });
}