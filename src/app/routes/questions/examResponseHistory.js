var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

module.exports = router => {
  router.get('/exams/:id/exams-response-history', async (req, res, next) => {
    try {
      let id = req.params.id;
      let wardId = req.query.w || 'all';

      let exam = await mongoose.model("exams").findById(id);     
      if (!exam) {
        // Invalid examID
        return next();
      }

      // filter res by examId
      let filter = { exam: ObjectId(id) }

      // Filter by ward Id.
      if (ObjectId.isValid(wardId) && wardId !== 'all') {
        filter.ward = ObjectId(wardId);
      }
      
      let resInfo = await mongoose.model('examResponses').find(filter)
                    .populate({path: 'ward', select: 'name'}).sort({createdDate: -1})
                    .select(['ward', 'exam', 'createdDate', 'numberCorrect']);
      let wards = await mongoose.model('wards').find();

      return res.render('statistic/examResponseHistory',  {resInfo, exam, wards, wardId});
    } catch (err) {
      next(err);  
    }
  });

  router.get('/exam-response/:id', async (req, res, next) => {
    let id = req.params.id;
    try {
      let pipline = [
        { $match: { _id: ObjectId(id)}}, 
        { $project: { answers: 1}}, 
        { $unwind: { path: "$answers", preserveNullAndEmptyArrays: true }}, 
        { $replaceRoot: { newRoot: "$answers" }}, 
        { $lookup: { from: 'questions', localField: 'question',
          foreignField: '_id', as: 'question'
        }},
        { $unwind: { path: "$question", preserveNullAndEmptyArrays: true }},
        { $lookup: { from: 'answers', localField: 'answer',
          foreignField: '_id', as: 'answer' }},
        { $unwind: { path: "$answer", preserveNullAndEmptyArrays: true}}
      ];
      
      let answerInfo = await mongoose.model('examResponses').aggregate(pipline);
      
      return res.render('statistic/answerHistory',  {answerInfo});
    } catch (err) {
      next(err);
    }
  });
}