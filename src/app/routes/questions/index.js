var router = require("express").Router();
var mongoose = require('mongoose');
require('./getAll')(router);
require('./exameCreate')(router);
require('./delete')(router);
require('./editExam')(router);
require('./changeExamStatus')(router);
require('./examResponseHistory')(router);

//wards join exam statistic page
router.get('/exam/numberofwards', async (req, res) => {
    try {
        let data = await mongoose.model('examResponses').aggregate([
            {
                $lookup: {
                    from: 'exams',
                    localField: 'exam',
                    foreignField: '_id',
                    as: 'exam_name'
                }
            },
            {
                $group: {
                    _id: '$exam',
                    wards: { $addToSet: '$ward' },
                    exam: { $first: '$exam_name' }
                }
            },
            {
                $project: {
                    wards: 1,
                    exam: { name: 1 }
                }
            }

        ])
        res.render('statistic', { data });
    } catch (error) {
        if (error) {
            console.log(error);
            throw error;
        }
    }
});

//number of users join exam 
router.get('/exam/:examID/numberofcandidates', async (req, res) => {
    let examID = req.params.examID;
    let examName = await mongoose.model('exams').findById(examID);
    let exam = await mongoose.model('exams').aggregate([
        {
            $match: {
                _id: mongoose.Types.ObjectId(examID)
            }
        },
        {
            $unwind: {
                path: '$questions',
                includeArrayIndex: "true"
            }
        },
        {
            $lookup:{
                from:'questions',
                localField:'questions',
                foreignField:'_id',
                as:'questionName'
            }
        },
        {
            $project:{
                questionName:1
            }
        },
        {
            $unwind:{
                path:'$questionName',
                includeArrayIndex:"true"
            }
        },
        {
            $project:{
                questionName:{_id:1,question:1,questionType:1}
            }
        }
    ])
    let data = await mongoose.model('examResponses').aggregate([
        {
            $match: {
                exam: mongoose.Types.ObjectId(examID)
            }
        },
        {
            $unwind: {
                path: '$answers',
                includeArrayIndex: "true"
            }
        },
        {
            $lookup: {
                from: 'questions',
                localField: 'answers.question',
                foreignField: '_id',
                as: 'question'
            }
        },

        {
            $unwind: {
                path: '$question',
                includeArrayIndex: "true"
            }
        },
        {
            $group: {
                _id: '$question._id',
                question: { $first: '$question' },
                count: { $sum: 1 }
            }
        },
        {
            $project: {
                count: 1,
                question: { question: 1, questionType: 1 }

            }
        }
    ])
    
    
    for(i=0;i<exam.length;i++){
        if(!isExits(data,exam[i].questionName._id)){
            let newQuestionWillBePush={
                _id: exam[i].questionName._id,
                question:{
                    question:exam[i].questionName.question,
                    questionType:exam[i].questionName.questionType
                },
                count:0
            }
            data.push(newQuestionWillBePush);
			
        }
        
    }
    res.render('statistic/numberofcandidates', { data, examName })
})

// //statistic number of users detail
// router.get('/exam/:examID/numberofcandidates/detail',async(req,res)=>{
//     let examID=req.params.examID;
//     let examName=await mongoose.model('exams').findById(examID)
//     let data=await mongoose.model('examResponses').aggregate([
//         {
//             $match:{
//                 exam:mongoose.Types.ObjectId(examID)
//             }
//         },
//         {
//             $lookup:{
//                 from:'wards',
//                 localField:'ward',
//                 foreignField:'_id',
//                 as:'wardName'
//             }            
//         },
//         {
//             $group:{
//                 _id:'$ward',
//                 count:{$sum:1},
//                 wardName:{$first:'$wardName'}
//             }
//         },
//         {
//             $unwind:{
//                 path:'$wardName',
//                 includeArrayIndex:"true"
//             }
//         }
//     ])
//     console.log(data);
//     res.render('statistic/questionDetail',{data,examName})
//     res.json('ok')
// })

const isExits=(array, id) =>{
    return array.some(function (el) {
        let a = JSON.stringify(el._id);
        let b = JSON.stringify(id);
        return a == b;
    });
}

module.exports = router;