var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var config = require("config/index");
var axios = require("axios");

var app = express();
app.set("topSecretKey", config.serectKey);

// view engine setup
app.set("views", path.join(__dirname, "src", "app", "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(
  express.static(path.join(__dirname, "src", "app", "public"), {
    maxAge: "30 days",
  })
);
app.set("Cache-Control", "max-age=3000");

// app.use("/", (req, res) => {
//   return res.send("hello from sms simulator");
// });

// app.use('')

function postFetch(url, params, header = {}, token) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, params, {
        cache: true,
        headers: {},
      })
      .then((res) => {
        if (res.status === 200) {
          resolve(res.data);
        } else {
          resolve({ error: true });
        }
      })
      .catch((e) => {
        console.log("errr");
        console.log(e);
        reject(e);
      });
  });
}

app.use("/send-sms", async (req, res, next) => {
  console.log("heloo");
  // const params = {
  //   Phone: phone,
  //   Content: content,
  //   ApiKey: serviceConfigs.apiKey,
  //   SecretKey: serviceConfigs.secretKey,
  //   SmsType: serviceConfigs.smsType,
  //   Brandname:
  //     serviceConfigs.smsType == 2 ? serviceConfigs.branchName : undefined,
  // };

  const hookUrl = `https://vietanenviro.webhook.office.com/webhookb2/b2fd13a4-ad5a-4945-86e6-d5d28aa3a5e1@3637655c-26b4-4101-b4bb-26e4268f09a6/IncomingWebhook/0a7031a81c9d48519fce9bc2a1739eef/69567316-1b22-4912-863d-c6ddfd38550f`;
  const { Phone, Content } = req.query;
  console.log({ Phone, Content });
  try {
    postFetch(hookUrl, {
      text: `Phone number: ${Phone} \n\n ${Content}`,
    });
  } catch (error) {
    console.log("Err cmdnd");
    console.log(error.message);
  }

  return res.send("hello from sms simulator");
});

app.use("/send-email", async (req, res, next) => {
  console.log("send email");
  // const params = {
  //   Phone: phone,
  //   Content: content,
  //   ApiKey: serviceConfigs.apiKey,
  //   SecretKey: serviceConfigs.secretKey,
  //   SmsType: serviceConfigs.smsType,
  //   Brandname:
  //     serviceConfigs.smsType == 2 ? serviceConfigs.branchName : undefined,
  // };

  const hookUrl = `https://vietanenviro.webhook.office.com/webhookb2/b2fd13a4-ad5a-4945-86e6-d5d28aa3a5e1@3637655c-26b4-4101-b4bb-26e4268f09a6/IncomingWebhook/0a7031a81c9d48519fce9bc2a1739eef/69567316-1b22-4912-863d-c6ddfd38550f`;
  const { from, to, cc, bcc, subject, text, html } = req.body;

  try {
    postFetch(hookUrl, {
      text: `From: ${from} \n\n To: ${to} \n\n CC: ${cc} \n\n BCC: ${bcc} \n\n Subject: ${subject} \n\n Text: ${text} \n\n Html: ${html} \n\n`,
    });
  } catch (error) {
    console.log("Err cmdnd");
    console.log(error.message);
  }

  return res.send("hello from sms simulator");
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  // next(createError(404));
  res.render("404");
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("500");
});

module.exports = app;
